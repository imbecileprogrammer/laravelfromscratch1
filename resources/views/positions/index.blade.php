@extends('layouts.master')

@section('content')
  <div class="container">
    <h1>Positions</h1>
    @if (Session::has('flash_msg'))
      <div class="alert alert-success">
          {{Session::get('flash_msg')}}
      </div>
    @endif
    <table class='table table-striped'>
      <thead>
        <tr>
          <th>Position Name</th>
          <th>Position Descripton</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($positions as $position)
        <tr>
          <td>{{$position->name}}</td>
          <td>{{$position->description}}</td>
          <td>
            <div class="col-md-2">
                <a href="{{route('positions.edit',$position->id)}}" class="btn btn-primary"> Edit</a>
            </div>

            <div class="col-md-1">
              {!! Form::open(['method' => 'DELETE', 'route' =>['positions.destroy',$position->id], 'class' => 'form-horizontal']) !!}

                {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}

              {!! Form::close() !!}
            </div>

          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$positions->render()}}
    <a href="{{route('positions.create')}}" class="btn btn-primary pull-right">Create Position</a>
  </div>

@endsection
