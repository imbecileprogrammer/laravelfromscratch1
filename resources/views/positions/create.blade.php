@extends('layouts.master')

@section('content')
  <div class="container">
  <h1>Create Position   <a href="{{route('positions.index')}}" class="btn-sm btn-primary">Back to Positions</a></h1>

  {!! Form::open(['method' => 'POST', 'route' => 'positions.store', 'class' => 'form-horizontal']) !!}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Position Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'Position Name') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
        {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
    </div>

  {!! Form::close() !!}
  
  </div>

@endsection
