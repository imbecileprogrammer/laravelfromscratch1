@extends('layouts.master')

@section('content')
<div class="container">
  <h1>Edit Category  <a href="{{route('categories.index')}}" class="btn-sm btn-primary">Back to Categories </a></h1>

{!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT']) !!}

<div class="col-md-8">
  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      {!! Form::label('name', 'Category Name') !!}
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      <small class="text-danger">{{ $errors->first('name') }}</small>
  </div>


  <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
      {!! Form::label('description', 'Category Description  ') !!}
      {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
      <small class="text-danger">{{ $errors->first('description') }}</small>
  </div>

  <div class="btn-group pull-right">
      {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
  </div>

</div>

{!! Form::close() !!}

</div>
@endsection
