@extends('layouts.master')

@section('content')
<div class="container">
  <h1>Categories</h1>
  @if(Session::has('flash_msg'))
    <div class="alert alert-success">
      {{Session::get('flash_msg')}}
    </div>
  @endif
<div class="col-md-10">

  <table class='table table-striped'>
    <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Action</th>

      </tr>
    </thead>
    <tbody>
      @foreach($categories as $category)
      <tr>
        <td>{{$category->name}}</td>
        <td>{{$category->description}}</td>
          <td>
            <div class="col-md-2">
                <a href="{{route('categories.edit',$category->id)}}" class="btn btn-primary">Edit</a>
            </div>
            <div class="col-md-1">
              {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy',$category->id], 'class' => 'form-horizontal']) !!}
              {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}

              {!! Form::close() !!}

            </div>


         </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  {{$categories->render()}}
<a href="{{route('categories.create')}}" class="btn btn-primary pull-right"> Create </a>
</div>


</div>
@endsection
