@extends('layouts.master')


@section('content')
  <div class="container">

      <div class="row">
          <div class="col-lg-12 text-center">
              <h1>Welcome!</h1>
              <p>
                {{$msg . ", " . $name}}
              </p>
          </div>
      </div>
      <!-- /.row -->

  </div>
@endsection
