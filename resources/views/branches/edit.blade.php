@extends('layouts.master')

@section('content')
<div class="container">
  <h1>Edit Category  <a href="{{route('branches.index')}}" class="btn-sm btn-primary">Back to Categories </a></h1>

{!! Form::model($branch, ['route' => ['branches.update', $branch->id], 'method' => 'PUT']) !!}

<div class="col-md-8">
  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      {!! Form::label('name', 'Branch Name') !!}
      {!! Form::text('name', null, ['class' => 'form-control']) !!}
      <small class="text-danger">{{ $errors->first('name') }}</small>
  </div>


  <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
      {!! Form::label('address', 'Branch Address ') !!}
      {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
      <small class="text-danger">{{ $errors->first('address') }}</small>
  </div>

  <div class="btn-group pull-right">
      {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
  </div>

</div>

{!! Form::close() !!}

</div>
@endsection
