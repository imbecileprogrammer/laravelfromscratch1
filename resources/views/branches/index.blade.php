@extends('layouts.master')

@section('content')
<div class="container">
  <h1>Categories</h1>
  @if(Session::has('flash_msg'))
    <div class="alert alert-success">
      {{Session::get('flash_msg')}}
    </div>
  @endif
<div class="col-md-10">

  <table class='table table-striped'>
    <thead>
      <tr>
        <th>Name</th>
        <th>Address</th>
        <th>Action</th>

      </tr>
    </thead>
    <tbody>
      @foreach($branches as $branch)
      <tr>
        <td>{{$branch->name}}</td>
        <td>{{$branch->address}}</td>
          <td>
            <div class="col-md-2">
                <a href="{{route('branches.edit',$branch->id)}}" class="btn btn-primary">Edit</a>
            </div>
            <div class="col-md-1">
              {!! Form::open(['method' => 'DELETE', 'route' => ['branches.destroy',$branch->id], 'class' => 'form-horizontal']) !!}
              {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}

              {!! Form::close() !!}

            </div>


         </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  {{$branches->render()}}
<a href="{{route('branches.create')}}" class="btn btn-primary pull-right"> Create </a>
</div>


</div>
@endsection
