<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('user', function () {
    return 'User Page';
});

Route::get('/','PagesController@welcome');
Route::get('/about','PagesController@about');
Route::get('/services','PagesController@services');
Route::get('/contact','PagesController@contact');



// ---------  Categories Route ----------------------

Route::resource('categories','CategoriesController');


// ---------  End Categories Route ----------------------


// ---------  Branches Route ----------------------

Route::get('/branches',[
  'as' => 'branches.index',
  'uses' => 'BranchesController@index'
]);

Route::get('/branches/create',[
  'as' => 'branches.create',
  'uses' => 'BranchesController@create'
]);

Route::get('/branches/{id}/edit',[
  'as' => 'branches.edit',
  'uses' => 'BranchesController@edit'
]);


Route::post('/branches/store',[
  'as' => 'branches.store',
  'uses' => 'BranchesController@store'
]);

Route::put('/branches/update/{id}',[
  'as' => 'branches.update',
  'uses' => 'BranchesController@update'
]);

Route::delete('/branches/delete/{id}',[
  'as' => 'branches.destroy',
  'uses' => 'BranchesController@destroy'
]);


// ---------  End Branches Route ----------------------


// ---------  Positions Route ----------------------

Route::get('/positions',[
  'as' => 'positions.index',
  'uses' => 'PositionsController@index'
]);

Route::get('/positions/create',[
  'as' => 'positions.create',
  'uses' => 'PositionsController@create'
]);

Route::get('/positions/{id}/edit',[
  'as' => 'positions.edit',
  'uses' => 'PositionsController@edit'
]);

Route::post('/positions',[
  'as' => 'positions.store',
  'uses' => 'PositionsController@store'
]);

Route::put('/positions/{id}',[
  'as' => 'positions.update',
  'uses' => 'PositionsController@update'
]);

Route::delete('/positions/{id}',[
  'as' => 'positions.destroy',
  'uses' => 'PositionsController@destroy'
]);

// ---------  End  Positions Route ----------------------
