<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Session;

class CategoriesController extends Controller
{
    public function index(){

      $categories = Category::paginate(4);

      return view('categories.index',compact('categories'));
    }

    public function create(){

      return view('categories.create');
    }


    public function edit($id){

      $category = Category::findOrFail($id);

      return view('categories.edit',compact('category'));
    }

    public function store(Request $request){

      $input = $request->all();

      $this->validate($request,[
        'name' => 'required|unique:categories',
        'description' => 'required'
      ]);


      Category::create($input)->save();
      Session::flash('flash_msg','Successfully Created Category!');

      return redirect('categories');
    }

    public function update($id,Request $request){
        $input = $request->all();
        $category = Category::findOrFail($id);

        $this->validate($request,[
          'name' => 'required',
          'description' => 'required'
        ]);

        $category->fill($input)->save();

        Session::flash('flash_msg','Successfully Updated Category!');

        return redirect('categories');


    }

    public function destroy($id){

     $category = Category::findOrFail($id);
     $category->delete();

     Session::flash('flash_msg','Successfully Deleted Category!');

     return redirect('categories');

    }
}
