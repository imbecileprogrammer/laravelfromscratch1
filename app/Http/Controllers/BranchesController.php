<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Branch;
use Session;

class BranchesController extends Controller
{
  public function index(){

    $branches = Branch::paginate(4);

    return view('branches.index',compact('branches'));
  }

  public function create(){

    return view('branches.create');
  }


  public function edit($id){

    $branch = Branch::findOrFail($id);

    return view('branches.edit',compact('branch'));
  }

  public function store(Request $request){

    $input = $request->all();

    $this->validate($request,[
      'name' => 'required|unique:branches',
      'address' => 'required'
    ]);

    Branch::create($input)->save();
    Session::flash('flash_msg','Successfully Created Branch!');

    return redirect('branches');
  }

  public function update($id,Request $request){
      $input = $request->all();
      $branch = Branch::findOrFail($id);

      $this->validate($request,[
        'name' => 'required',
        'address' => 'required'
      ]);

      $branch->fill($input)->save();

      Session::flash('flash_msg','Successfully Updated Branch!');

      return redirect('branches');


  }

  public function destroy($id){

   $branch = Branch::findOrFail($id);
   $branch->delete();

   Session::flash('flash_msg','Successfully Deleted Branch!');

   return redirect('branches');

  }
}
