<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
     public function welcome(){
       $msg = "Hello World";
       $name = "batman";
       return view('welcome',compact('msg','name'));
     }

     public function about(){

       return view('about');

     }

     public function services(){
       
       $services = ['Android Development','Desktop Application','Ecommerce'];
       return view('services',compact('services'));

     }

     public function contact(){

       return view('contact');

     }
}
