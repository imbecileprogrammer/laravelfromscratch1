<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Position;
use Session;

class PositionsController extends Controller
{

    public function index()
    {
      $positions = Position::paginate(4);

      return view('positions.index',compact('positions'));

    }


    public function create()
    {
       return view('positions.create');
    }


    public function store(Request $request)
    {
      $input = $request->all();
      
      $this->validate($request,[
        'name' => 'required',
        'description' =>'required'
      ]);

      Position::create($input)->save();
      Session::flash('flash_msg','Successfully Created Positiosn');
      return redirect('positions');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
      $position = Position::findOrFail($id);
      return view('positions.edit',compact('position'));
    }


    public function update(Request $request, $id)
    {
       $input = $request->all();

       $this->validate($request,[
         'name' => 'required',
         'description' =>'required'
       ]);

       $position = Position::findOrFail($id);
       $position->fill($input)->save();

       Session::flash('flash_msg','Successfully Updated Position');
       return redirect('positions');
    }

    public function destroy($id)
    {
        $position = Position::findOrFail($id);
        $position->delete();

        Session::flash('flash_msg','Successfully Deleted Position');
        return redirect('positions');
    }
}
