<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public $table = "branches";
    public $fillable = ['name','address'];
}
