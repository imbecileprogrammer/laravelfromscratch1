<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $fillable = ['first_name','middle_name','last_name','birthdate','position_id'];

    public function position(){
      return $this->hasOne('App\Position', 'position_id', 'id');
    }

}
